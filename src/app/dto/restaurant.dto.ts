export class RestaurantDto {

  id?: number;
  restaurantName: string;
  restaurantAddress: string;
  restaurantEmail: string;
  restaurantPhone: string;
}
